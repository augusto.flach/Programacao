/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bigolin.pizzaria.model.testes;

import java.util.ArrayList;
import org.bigolin.pizzaria.model.Ingrediente;
import org.bigolin.pizzaria.model.Pizza;


public class JavaApplication10 {

    public static void main(String[] args)  {
        
        Pizza p = new Pizza();
        p.setIdPizza(14);
        p.setPeso(200);
        p.setNome("Bomba");
        p.setTamanho('G');
        p.setValorBase(20.50);
    
        
        Ingrediente cal = new Ingrediente();
        cal.setNome("Calabresa");
        cal.setQuantidade(100);//gramas
         
        Ingrediente i2 = new Ingrediente();
        i2.setNome("Rucula");
        i2.setQuantidade(20);   
        
        p.addIngrediente(cal);
        p.addIngrediente(i2);
        
        p.insert();
     
        Pizza p2 = new Pizza();
        p.setIdPizza(14);
        p.setPeso(200);
        p.setNome("Ahola");
        p.setTamanho('G');
        p.setValorBase(20.50);
    
        
        Ingrediente cal2 = new Ingrediente();
        cal.setNome("Calabresa");
        cal.setQuantidade(150);//gramas
        
        Ingrediente i3 = new Ingrediente();
        i3.setNome("Mortadela");
        i3.setQuantidade(20);   
        
        p2.addIngrediente(cal);
        p2.addIngrediente(i2);
        
        p2.insert();
        
        ArrayList<Pizza> pizzas  = Pizza.getAll();
        
        for(Pizza a : pizzas){
            System.out.println(a);//imprimir a pizza com os seus respectivos ingredientes
        }
        
        p2.setNome("aloha2");
        p2.update();
        p.delete();
        
        pizzas  = Pizza.getAll();
        
        for(Pizza a : pizzas){
            System.out.println(a);//imprimir a pizza com os seus respectivos ingredientes
        }

    }

}