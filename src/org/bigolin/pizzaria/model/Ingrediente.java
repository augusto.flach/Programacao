/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bigolin.pizzaria.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.bigolin.ActiveRecord;
import org.bigolin.Conexao;

/**
 *
 * @author Aluno
 */
public class Ingrediente implements ActiveRecord{
    private int id;
    private String nome;
    private int quantidade;

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public boolean insert() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;
        boolean t = false;
        String itSQL = "Select * from ingrediente i where upper(i.nome) = upper (?)";  
      
        try {
            ps = dbConnection.prepareStatement(itSQL);
            ps.setString(1, this.nome);
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                id= rs.getInt("id");
            }else{
                itSQL = "Select max(id)+1 from ingrediente";
                Statement s = dbConnection.createStatement();
                rs = s.executeQuery(itSQL);
                if(rs.next()) {
                    id = rs.getInt("max(id)+1");
                }
                // observe que uso o método execute update
                itSQL = "Insert into ingrediente (id,nome) values (?,?)";
                ps = dbConnection.prepareStatement(itSQL);
                ps.setInt(1, id);
                ps.setString(2,nome);
                ps.executeUpdate();
            }
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }finally{
                c.desconecta(); //Boa pratica
            }        
        
        return true;
    }

    @Override
    public boolean delete() {
        return false;
    }

    @Override
    public boolean update() {
        return false;
    }

    @Override
    public void load() {
        return false;
    }
    
}
