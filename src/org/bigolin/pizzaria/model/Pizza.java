/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bigolin.pizzaria.model;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import org.bigolin.ActiveRecord;
import org.bigolin.Conexao;

/**
 *
 * @author marcio
 */
public class Pizza implements ActiveRecord {
    private int idPizza;
    private int peso;
    private String nome;
    private final ArrayList<Ingrediente> i = new ArrayList<>(); 
   private double valorBase;
    private char tamanho;
    
    public int getIdPizza() {
        return idPizza;
    }

    public void setIdPizza(int id) {
        this.idPizza = id;
    }

    public int getPeso() {
        return peso;
    }

    public void setPeso(int peso) {
        this.peso = peso;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getValorBase() {
        return valorBase;
    }

    public void setValorBase(double valorBase) {
        this.valorBase = valorBase;
    }

    public char getTamanho() {
        return tamanho;
    }

    public void setTamanho(char tamanho) {
        this.tamanho = tamanho;
    }  
        
    

    public void load() {
        String selectSQL = "SELECT * FROM pizza WHERE id_pizza = ?";
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();

        PreparedStatement ps;
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, this.idPizza);// testar para ver se o id esta setado

            ResultSet rs = ps.executeQuery();// Observe que não passo nenhum
                                        // argumento
            if (rs.next()) {//verifico para não dar nullPointerException
                this.setNome(rs.getString("nome_pizza"));  // Observe que a
                                                                // string que vai de arg é o nome da coluna
                this.setTamanho(rs.getString("tamanho").charAt(0));
                this.setValorBase(rs.getDouble("valor_base"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {c.desconecta();}

    }

    @Override
    public boolean insert() {
	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;

        String insertTableSQL = "INSERT INTO pizza" + "(id_pizza, nome_pizza, tamanho, valor_base) VALUES" + "(?,?,?,?)";
        

        try {
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setInt(1, this.idPizza);
            preparedStatement.setString(2, this.nome);
            preparedStatement.setString(3, String.valueOf(this.tamanho));
            preparedStatement.setDouble(4, this.valorBase);
          
            // observe que uso o método execute update
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }finally{
            c.desconecta(); //Boa pratica
        }        
        return true;
    }
    

    @Override
    public boolean delete() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        String insertTableSQL = "DELETE FROM pizza WHERE id_pizza = ?)";        

        try {
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setInt(1, this.idPizza);
          
            // observe que uso o método execute update
            ps.executeUpdate();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }finally{
            c.desconecta(); //Boa pratica
        }        
        return true;
        
    }

    @Override
    public boolean update() {
      	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        String insertTableSQL = "UPDATE pizza SET nome_pizza =?,tamanho = ?, valor_base=? WHERE id_pizza = ?";
        

        try {
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setInt(4, this.idPizza);
            ps.setString(1, this.nome);
            ps.setString(2, String.valueOf(this.tamanho));
            ps.setDouble(3, this.valorBase);
          
            // observe que uso o método execute update
            ps.executeUpdate();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }finally{
            c.desconecta(); //Boa pratica
        }        
        return true;
    }

    public static ArrayList<Pizza> getAll() {
        String selectSQL = "SELECT * FROM pizza";
        ArrayList<Pizza> lista = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        Statement st;
        try {
            /*
			 * Observe que como não tenho nenhuma variável não devo usar
			 * PreparedStatement
             */
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);// Observe que a query foi passada como arg
            while (rs.next()) {
                Pizza f = new Pizza();
               // Mesma xorna la de cima ver um método mais inteligent
                lista.add(f);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally{
            c.desconecta(); //Boa pratica
        }   

        return lista;
    }
    
    public boolean addIngrediente(Ingrediente in){
        if(!i.contains(in)) i.add(in); else return false; return true;
    }
    public boolean removeIngrediente(Ingrediente in){
        if(i.contains(in)) i.remove(in); else return false; return true;
        
    }


}
