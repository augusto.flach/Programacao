package org.bigolin;


public interface ActiveRecord {

    public boolean insert();
    
    public boolean delete();
    
    public boolean update();
    
    public void load();

    
}
